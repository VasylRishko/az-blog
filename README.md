# Simple blog application for AZinec

Prod URL: https://az-blog.herokuapp.com/

Prod API URL: https://az-blog.herokuapp.com/api

## Tech stack

- node.js - `6.12.0`
- yarn - `1.3.2`
- mongodb - `3.2.10`
- express.js - `4.16.x`

## Run
1. `npm i yarn -g`
1. `yarn install`
1. `yarn run server`

## Test env

- mocha - `4.x`
- chai - `4.x`
- supertest - `3.x`


Run tests - `yarn test`
