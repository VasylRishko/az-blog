const { pick } = require('lodash');
const errorHandler = require('mongoose-error-handler');
const { ObjectId } = require('mongodb');
const isValidId = require('mongoose').Types.ObjectId.isValid;
const Comment = require('../models/comment');
const Post = require('../models/post');

const COMMENT_ATTRS = ['body', 'post'];

module.exports = {
  create: function (req, res ,next) {
    if (!isValidId(req.body.post)) {
      return res.status(404).json({ message: 'Post not found' });
    }

    Post.findOne({ _id: req.body.post })
      .exec((err, post) => {
        if (err) return next(err);
        if (!post) return res.status(404).json({ message: 'Post not found' });

        createComment(post)
      });

    function createComment (post) {
      const commentAttrs = pick(req.body, COMMENT_ATTRS);
      commentAttrs.user = req.user.id;

      Comment.create(commentAttrs, (err, newComment) => {
        if(err) {
          return res.status(422).json(errorHandler.set(err))
        }

        post.comments.push(newComment);

        post.save((postErr, post) => {
          if (postErr) return next(postErr);

          res.json({ comment: newComment })
        })
      })
    }
  },

  delete: function (req, res ,next) {
    if (!isValidId(req.params.id)) {
      return res.status(404).json({ message: 'Comment not found' });
    }

    const userId = ObjectId(req.user.id);
    const query = { user: userId, _id: req.params.id };

    Comment.findOneAndRemove(query)
      .exec((err, comment) => {
        if (err) return next(err);
        if(!comment) {
          return res.status(404).json({ message: 'Comment not found' })
        }

        res.json({ message: 'Comment was deleted' })
      })
  },
};
