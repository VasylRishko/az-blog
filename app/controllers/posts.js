const { pick, concat } = require('lodash');
const errorHandler = require('mongoose-error-handler');
const { ObjectId } = require('mongodb');
const isValidId = require('mongoose').Types.ObjectId.isValid;

const Post = require('../models/post');
const Comment = require('../models/comment');
const POST_ATTRS = ['title', 'body'];

module.exports = {
  index: function (req, res, next) {
    Post.find()
      .sort({ createdAt: -1 })
      .select({ title: 1, body: 1, user: 1, createdAt: 1 })
      .populate('user', { firstName: 1, lastName: 1, username: 1 })
      .populate('comments', { id: 1 })
      .exec((err, posts) => {
        if(err) return next(err);

        res.json({ posts: posts })
      })
  },

  show: function (req, res ,next) {
    const { id } = req.params;

    if (!isValidId(id)) {
      return res.status(404).json({ message: 'Post not found' });
    }

    Post.findOne({ _id: id })
      .select({ title: 1, body: 1, user: 1, createdAt: 1 })
      .exec((err, post) => {
        if(err) return next(err);
        if(!post) return res.status(404).json({ message: 'Post not found' });

        loadCommentsWithAuthors(post)
      });

    function loadCommentsWithAuthors (post) {
      Comment.find({ post: post._id })
        .sort({ createdAt: -1 })
        .select({ body: 1, user: 1, createdAt: 1 })
        .populate('user', { username: 1 })
        .exec((err, comments) => {
          if(err) return next(err);

          post.comments = comments;

          res.json({ post: post })
        })
    }
  },

  create: function (req, res ,next) {
    const postAttrs = pick(req.body, POST_ATTRS);
    postAttrs.user = req.user.id;

    Post.create(postAttrs, (err, newPost) => {
      if(err) {
        return res.status(422).json(errorHandler.set(err))
      }

      res.json({ post: newPost })
    })
  },

  edit: function (req, res ,next) {
    if (!isValidId(req.params.id)) {
      return res.status(404).json({ message: 'Post not found' });
    }

    const userId = ObjectId(req.user.id);
    const query = { user: userId, _id: req.params.id };
    const update = pick(req.body, ['title', 'body']);

    Post.findOneAndUpdate(query, update, { new: true })
      .exec((err, post) => {
        if (err) return next(err);
        if(!post) return res.status(404).json({ message: 'Post not found' });

        res.json({ post: post })
      })
  },

  delete: function (req, res ,next) {
    if (!isValidId(req.params.id)) {
      return res.status(404).json({ message: 'Post not found' });
    }

    const userId = ObjectId(req.user.id);
    const query = { user: userId, _id: req.params.id };

    Post.findOneAndRemove(query)
      .exec((err, post) => {
        if (err) return next(err);
        if(!post) return res.status(404).json({ message: 'Post not found' });

        res.json({ message: 'Post was deleted' })
      })
  }
};
