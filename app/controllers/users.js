const { pick, concat } = require('lodash');
const errorHandler = require('mongoose-error-handler');
const jwt = require('../../lib/jwt');
const User = require('../models/user');
const Post = require('../models/post');

const USER_ATTRS = ['firstName', 'lastName', 'username', 'email', 'password'];

module.exports = {
  index: function (req, res, next) { },

  edit: function (req, res ,next) { },

  delete: function (req, res ,next) { },

  show: function (req, res ,next) {
    res.json({ user: req.user })
  },

  create: function (req, res ,next) {
    const userAttrs = pick(req.body, USER_ATTRS);

    User.create(userAttrs, (err, newUser) => {
      if(err) {
        return res.status(422).json(errorHandler.set(err))
      }

      let resJSON = { user: newUser };

      if (req.query.auth) {
        resJSON.jwt = jwt.sign(newUser);
      }

      res.json(resJSON);
    })
  },

  token: function (req, res ,next) {
    const { email, password } = req.query;

    User.findOne({ email: email })
      .exec((err, user) => {
        if (err) return next(err);
        if (!user) return res.status(404).json({ message: 'User not found' });

        authorize(user);
      });

    function authorize (user) {
      user.comparePassword(password, function (err, isMatch) {
        if (err) return next(err);
        if (!isMatch) {
          return res.status(422).json({ message: 'User password is incorrect' })
        }

        const jwtData = jwt.sign(user);

        return res.status(200).json({ jwt: jwtData, user: user })
      });
    }
  },

  posts: function (req, res ,next) {
    Post.find({ user: req.user.id })
      .sort({ createdAt: -1 })
      .select({ title: 1, body: 1, user: 1, createdAt: 1 })
      .populate('comments', { id: 1 })
      .exec((err, posts) => {
        if(err) return next(err);

        res.json({ posts: posts })
      })
  }
};
