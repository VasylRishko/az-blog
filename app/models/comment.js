const { pick } = require('lodash');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqErrorsHandler = require('mongoose-beautiful-unique-validation');

const CommentSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: 'User relation is required'
  },
  post: {
    type: Schema.Types.ObjectId,
    ref: 'Post',
    required: 'Post relation is required'
  },

  body: {
    type: String,
    required: 'Text is required'
  },
}, {
  timestamps: true,
  toJSON: {
    transform: function(doc, ret) {
      return pick(doc, ['id', 'user', 'post', 'body', 'createdAt', 'updatedAt']);
    }
  }
});

CommentSchema.plugin(uniqErrorsHandler);

module.exports = mongoose.model('Comment', CommentSchema);
