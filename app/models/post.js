const { pick } = require('lodash');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqErrorsHandler = require('mongoose-beautiful-unique-validation');

const PostSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: 'User relation is required'
  },
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'Comment'
  }],

  title: {
    type: String,
    required: 'Title is required'
  },
  body: {
    type: String,
    required: 'Body is required'
  },
}, {
  timestamps: true,
  toJSON: {
    transform: function (doc, ret) {
      return pick(doc, ['id', 'createdAt', 'updatedAt', 'user', 'comments',
        'title', 'body'
      ])
    }
  }
});

PostSchema.plugin(uniqErrorsHandler);

module.exports = mongoose.model('Post', PostSchema);
