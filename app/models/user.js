const { pick } = require('lodash');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqErrorsHandler = require('mongoose-beautiful-unique-validation');
const { hash, compare } = require('bcrypt');
const SALT_ROUNDS = 10;

const UserSchema = new Schema({
  posts: [{
    type: Schema.Types.ObjectId,
    ref: 'Post'
  }],

  firstName: String,
  lastName: String,
  username: {
    type: String,
    required: 'Username is required',
    lowercase: true,
    trim: true
  },
  email: {
    type: String,
    required: 'Email is required',
    unique: 'User this email "{VALUE}" already exists',
    match: [
      /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
      'Email is invalid'
    ]
  },
  password: {
    type: String,
    required: 'Password is required',
    minlength: [8, 'Password should have 8 symbols minimum'],
    match: [
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/,
      'Password requires at least one uppercase letter, one lowercase letter, one number and one special character'
    ]
  },
}, {
  timestamps: true,
  toJSON: {
    transform: function(doc, ret) {
      return pick(doc, ['id', 'firstName', 'lastName', 'username', 'email',
        'createdAt', 'updatedAt'
      ]);
    }
  }
});

UserSchema.pre('save', function(next) {
  hash(this.password, SALT_ROUNDS, (err, hash) => {
    if (err) return next(err);

    this.password = hash;
    next();
  });
});

UserSchema.methods.comparePassword = function(password, cb) {
  compare(password, this.password, (err, isMatch) => {
    if (err) return cb(err);

    cb(null, isMatch);
  });
};

UserSchema.plugin(uniqErrorsHandler);

module.exports = mongoose.model('User', UserSchema);
