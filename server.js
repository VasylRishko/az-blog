const app = require('./app');
const config = require('./config');
const mongoose = require('mongoose');
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'development';

mongoose.Promise = require('bluebird');

connectDB()
  .on('error', console.error)
  .once('open', startServer);

function connectDB () {
  const options = { useMongoClient: true };
  return mongoose.connect(config.dbURL, options);
}

function startServer() {
  if (env === 'test') return;

  app.listen(port, () => {
    console.log('Server started on port: %s, environment: %s ...\n', port, env)
  });
}

module.exports = app;
