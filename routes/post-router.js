const PostRouter = require('express').Router();
const PostController = require('../app/controllers/posts');

module.exports = function (authenticate) {
  PostRouter
    .get('/', PostController.index)
    .get('/:id', PostController.show)
    .post('/', authenticate(), PostController.create)
    .put('/:id', authenticate(), PostController.edit)
    .patch('/:id', authenticate(), PostController.edit)
    .delete('/:id',authenticate(),  PostController.delete);

  return PostRouter
};
