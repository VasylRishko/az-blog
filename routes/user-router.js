const UserRouter = require('express').Router();
const UserController = require('../app/controllers/users');

module.exports = function (authenticate) {
  UserRouter
    .get('/token', UserController.token)
    .post('/', UserController.create)
    .get('/posts', authenticate(), UserController.posts)
    .get('/:id', authenticate(), UserController.show);
  
  return UserRouter
};
