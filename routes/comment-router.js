const CommentRouter = require('express').Router();
const CommentController = require('../app/controllers/comments');

module.exports = function (authenticate) {
  CommentRouter
    .post('/', authenticate(), CommentController.create)
    .delete('/:id', authenticate(), CommentController.delete);

  return CommentRouter
};
