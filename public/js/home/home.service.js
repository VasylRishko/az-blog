angular
  .module('azBlog.home.service', [])
  .service('HomeService', [
    '$rootScope', '$http',
    function ($rootScope, $http) {

      this.loadPosts = function () {
        return $http({
          method: 'GET',
          url: '/api/posts',
          headers: { 'Content-Type': 'application/json' }
        })
      };

    }
  ]);
