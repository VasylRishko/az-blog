angular
  .module('azBlog.home', ['ngRoute'])
  .controller('HomeCtrl', [
    '$scope', 'HomeService',
    function ($scope, HomeService) {
      HomeService
        .loadPosts()
        .then(function (response) {
          $scope.posts = response.data.posts;
        })
        .catch(function () {
          Materialize.toast('Ups, something went wrong', 4000)
        });
    }
  ]);
