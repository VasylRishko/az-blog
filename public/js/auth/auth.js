angular
  .module('azBlog.auth', ['ngRoute', 'angularValidator'])
  .controller('AuthCtrl', [
    '$rootScope', '$scope', '$location', 'AuthService',
    function ($rootScope, $scope, $location, AuthService) {
      $scope.login = function () {
        AuthService.login($scope.candidate)
          .then(function (result) {
            $location.path('/')
          }).catch(function (err) {
            if (err.data && err.data.message)
              Materialize.toast(err.data.message, 4000)
          })
      };

      $scope.signup = function () {
        AuthService.signup($scope.newUser)
          .then(function (result) {
            Materialize.toast('User created', 4000);
            $location.path('/')
          }).catch(function (err) {
            if (err.data && err.data.errors && err.data.errors) {
              var msg = Object.values(err.data.errors).join('</br>');
              Materialize.toast(msg, 4000)
            }
          })
      };
    }
  ]);
