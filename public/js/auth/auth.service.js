angular
  .module('azBlog.auth.service', [])
  .service('AuthService', [
    '$rootScope', '$http',
    function ($rootScope, $http) {

      this.login = function (user) {
        return $http({
          method: 'GET',
          url: '/api/users/token',
          params: user,
          headers: {'Content-Type': 'application/json'}
        }).then(loginUser);
      };

      this.signup = function (user) {
        return $http({
          method: 'POST',
          url: '/api/users',
          data: user,
          params: { auth: true },
          headers: {'Content-Type': 'application/json'}
        }).then(loginUser);
      };

      this.loadUser = function (token) {
        return $http({
          method: 'GET',
          url: '/api/users/me',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
          }
        }).then(loginUser);
      };

      function loginUser (response) {
        $rootScope.isUserIn = true;
        $rootScope.currentUser = response.data.user;

        var jwt = response.data.jwt;

        if (!!jwt) {
          Cookies.set('token', jwt.token);
        }
      }
    }
  ]);
