angular
  .module('azBlog.config', ['ngRoute'])
  .config([
    '$routeProvider',
    function ($routeProvider) {
      $routeProvider.when('/', {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl',
        authenticate: false,
      })
      .when('/login', {
        templateUrl: 'templates/auth/login.html',
        controller: 'AuthCtrl',
        authenticate: false,
      })
      .when('/signup', {
        templateUrl: 'templates/auth/signup.html',
        controller: 'AuthCtrl',
        authenticate: false,
      })
      .when('/account', {
        templateUrl: 'templates/account/account.html',
        controller: 'AccountCtrl',
        authenticate: true,
      })
      .when('/posts/new', {
        templateUrl: 'templates/post/new.html',
        controller: 'PostCtrl',
        authenticate: true,
      })
      .when('/posts/:id', {
        templateUrl: 'templates/post/show.html',
        controller: 'PostCtrl',
        authenticate: false,
      })
      .when('/posts/edit/:id', {
        templateUrl: 'templates/post/edit.html',
        controller: 'PostCtrl',
        authenticate: false,
      })
      .otherwise({ redirectTo: '/' });
    }
  ])
  .run([
    '$rootScope', '$location', '$route', 'AuthService',
    function ($rootScope, $location, $route, AuthService) {
      $rootScope.$on('$routeChangeStart', function (e, next, current) {
        // Check authorization !
        var token = Cookies.get('token');

        if (next.authenticate)
          authenticateUser(token);

        setCurrentUser(token);
      });

      function authenticateUser (token) {
        if (!token)
          return failedRedirect();
      }

      function setCurrentUser (token) {
        if (!!token && !$rootScope.currentUser) {
          AuthService
            .loadUser(token)
            .catch(function (err) {
              Cookies.remove('token');
              failedRedirect()
            })
        }
      }

      function failedRedirect () {
        $rootScope.currentUser = null;
        $rootScope.isUserIn = false;
        $location.path('/login');
      }
    }
  ]);
