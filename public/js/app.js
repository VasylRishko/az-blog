angular
  .module('azBlog', [
    'ngRoute',
    'azBlog.config',
    'azBlog.helpers',
    'azBlog.auth',
    'azBlog.auth.service',
    'azBlog.home',
    'azBlog.home.service',
    'azBlog.post',
    'azBlog.post.service',
    'azBlog.account',
    'azBlog.comment.service',
    'azBlog.components.navLinks',
    'azBlog.components.commentForm',
  ]);
