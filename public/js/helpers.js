angular
  .module('azBlog.helpers', [])
  .run([
    '$rootScope',
    function ($rootScope, $location, $route, AuthService) {
      $rootScope.dateFormat = function (date) {
        var result = '',
          mDate = moment(date);

        if (moment().diff(mDate, 'days') > 0) {
          result = mDate.format('MMMM Do YYYY, h:mm a');
        } else {
          result = mDate.from(moment());
        }
        return result
      }
    }
  ]);
