angular
  .module('azBlog.post', ['ngRoute'])
  .controller('PostCtrl', [
    '$scope', '$rootScope', '$location', '$routeParams', 'PostService',
    function ($scope, $rootScope, $location, $routeParams, PostService) {
      loadPost();

      $scope.createPost = function () {
        PostService.create($scope.newPost)
          .then(function () {
            $location.path('/')
          })
          .catch(function (err) {
            if (err.data && err.data.errors && err.data.errors) {
              var msg = Object.values(err.data.errors).join('</br>');
              Materialize.toast(msg, 4000)
            }
          })
      };

      $scope.editPost = function () {
        PostService
          .update($scope.post)
          .then(function () {
            $location.path('/account')
          })
          .catch(function (err) {
            if (err.data && err.data.errors && err.data.errors) {
              var msg = Object.values(err.data.errors).join('</br>');
              Materialize.toast(msg, 4000)
            }
          })
      };

      function loadPost() {
        if (!$routeParams.id) return;

        PostService
          .show($routeParams.id)
          .then(function (response) {
            $scope.post = response.data.post;
          })
          .catch(function (err) {
            if (err.data && err.data.message) {
              Materialize.toast(err.data.message, 4000);
              $location.path('/')
            }
          });

        $scope.visibleElements = {
          commentFormVisible: false,
          commentBtnVisible: !!$rootScope.isUserIn
        };

        $scope.openCommentForm = function () {
          $scope.visibleElements.commentFormVisible = true;
          $scope.visibleElements.commentBtnVisible = false;
        };

        setTimeout(Materialize.updateTextFields, 100);
      }
    }
  ]);
