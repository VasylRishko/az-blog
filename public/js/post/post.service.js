angular
  .module('azBlog.post.service', [])
  .service('PostService', [
    '$rootScope', '$http',
    function ($rootScope, $http) {

      this.create = function (post) {
        return $http({
          method: 'POST',
          url: '/api/posts',
          data: post,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + Cookies.get('token')
          }
        })
      };

      this.show = function (id) {
        return $http({
          method: 'GET',
          url: '/api/posts/' + id,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + Cookies.get('token')
          }
        })
      };

      this.update = function (post) {
        return $http({
          method: 'PUT',
          data: post,
          url: '/api/posts/' + post.id,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + Cookies.get('token')
          }
        })
      };

      this.userPosts = function () {
        return $http({
          method: 'GET',
          url: '/api/users/posts',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + Cookies.get('token')
          }
        })
      };

      this.delete = function (id) {
        return $http({
          method: 'DELETE',
          url: '/api/posts/' + id,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + Cookies.get('token')
          }
        })
      };

    }
  ]);
