angular
  .module('azBlog.account', ['ngRoute', 'angularValidator'])
  .controller('AccountCtrl', [
    '$rootScope', '$scope', '$location', 'PostService',
    function ($rootScope, $scope, $location, PostService) {

      PostService
        .userPosts()
        .then(function (response) {
          $scope.userPosts = response.data.posts
        })
        .catch(function () {
          Materialize.toast('Ups, something went wrong', 4000)
        });

      $scope.deletePost = function (id) {
        var confirmed = confirm('Are you sure?');

        if (!confirmed) return false;


        PostService
          .delete(id)
          .then(function (response) {
            Materialize.toast('Post was deleted', 4000);
            _.remove($scope.userPosts, { id: id })
          })
          .catch(function () {
            Materialize.toast('Ups, something went wrong', 4000)
          });
      }
    }
  ]);
