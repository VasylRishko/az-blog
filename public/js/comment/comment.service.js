angular
  .module('azBlog.comment.service', [])
  .service('CommentService', [
    '$rootScope', '$http',
    function ($rootScope, $http) {

      this.create = function (comment) {
        return $http({
          method: 'POST',
          url: '/api/comments',
          data: comment,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + Cookies.get('token')
          }
        })
      };

    }
  ]);
