angular
  .module('azBlog.components.commentForm', [])
  .controller('CommentFormCtrl', [
    '$rootScope', '$scope', '$location', 'CommentService',
    function ($rootScope, $scope, $location, CommentService) {
      $scope.createComment = function () {
        var comment = {
          post: $scope.post.id,
          body: $scope.body
        };

        CommentService
          .create(comment)
          .then(function (response) {
            var newComment = response.data.comment;

            newComment.user = $rootScope.currentUser;
            $scope.post.comments.push(newComment);

            $scope.visibleElements.commentFormVisible = false;
            $scope.visibleElements.commentBtnVisible = true;
          })
          .catch(function (err) {
            if (err.data && err.data.message)
            Materialize.toast(err.data.message, 4000)
          })
      }
    }
  ])
  .directive('commentForm', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/components/comment-form.html',
    };
  });
