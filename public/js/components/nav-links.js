angular
  .module('azBlog.components.navLinks', [])
  .controller('NavLinksCtrl', [
    '$rootScope', '$scope', '$location',
    function ($rootScope, $scope, $location) {
      $rootScope.isUserIn = !!$rootScope.currentUser;

      $scope.logout = function (e) {
        $rootScope.currentUser = null;
        $rootScope.isUserIn = false;

        Cookies.remove('token');
        $location.path('/')
      }
    }
  ])
  .directive('navLinks', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/components/nav-links.html',
    };
  });
