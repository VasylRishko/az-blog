const bodyParser = require('body-parser');

module.exports.configure = function (app) {
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
};
