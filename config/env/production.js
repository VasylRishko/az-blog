module.exports = {
  dbURL: 'mongodb://localhost/az_blog_development',
  jwtSecret:' process.env.JWT_SECRET',
  jwtExpiresIn: 3 * 24 * 60 * 60 // 3 days
};
