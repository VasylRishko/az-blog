module.exports = {
  dbURL: 'mongodb://localhost/az_blog_test',
  jwtSecret: 'test',
  jwtExpiresIn: 60 * 60 // 1 hour
};
