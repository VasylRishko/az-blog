module.exports = {
  dbURL: 'mongodb://localhost/az_blog_development',
  jwtSecret: 'c3VwZXItc2VjcmV0Oi0p',
  jwtExpiresIn: 3 * 24 * 60 * 60 // 3 days
};
