const { notFoundHandler } = require('express-api-error-handler');

module.exports.handle = function (app) {
  app.use(notFoundHandler({ hideProdErrors: true }));
};
