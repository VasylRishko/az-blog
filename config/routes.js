const prefix = 'api';
const UserRouter = require('../routes/user-router');
const PostRouter = require('../routes/post-router');
const CommentRouter = require('../routes/comment-router');

module.exports = {
  prefix: prefix,

  configure: function (app, passport) {
    app.use(`/${prefix}/users`, UserRouter(authenticate));
    app.use(`/${prefix}/posts`, PostRouter(authenticate));
    app.use(`/${prefix}/comments`, CommentRouter(authenticate));

    function authenticate () {
      return passport.authenticate('bearer', { session: false })
    }
  }
};
