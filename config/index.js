const { assign } = require('lodash');
const nodeEnv = process.env.NODE_ENV || 'development';
const env = require(`./env/${nodeEnv}`);

const defaultConfigs = {
  dbURL: '',
  jwtSecret: '',
  jwtExpiresIn: 60 // in seconds
};

module.exports = assign(defaultConfigs, env);
