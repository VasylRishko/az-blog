const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const { verify } = require('../lib/jwt');
const User = require('../app/models/user');
const isValidId = require('mongoose').Types.ObjectId.isValid;

const strategy = new Strategy(function(token, cb) {
  verify(token, (tokenErr, payload) => {
    if (tokenErr) return cb(null, false);

    loadUser(payload && payload.id)
  });

  function loadUser (userId) {
    if (!isValidId(userId)) {
      return cb(null, false)
    }

    User.findOne({ _id: userId })
      .exec((err, user) => {
        if (err) return cb(err);
        if(!user) return cb(null, false);

        return cb(null, user)
      });
  }
});

module.exports = passport.use(strategy);
