const morgan = require('morgan');

module.exports.configure = function (app) {
  if (app.get('env') === 'test') return;

  app.use(morgan('dev'))
};
