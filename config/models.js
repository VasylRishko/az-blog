const fs = require('fs');
const { join } = require('path');
const modelsPath = join(__dirname, '../app/models');
const { forEach } = require('lodash');

module.exports = {
  load: function () {
    const models = fs.readdirSync(modelsPath);

    forEach(models, (m) => require(`${modelsPath}/${m}`))
  }
};
