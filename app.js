const env = process.env.NODE_ENV || 'development';
const express = require('express');
const app = express();
// TODO: bundle assets for production !
// const staticServe = express.static(env === 'production' ? 'dist' : 'public');
const staticServe = express.static('public');

const models = require('./config/models');
const logger = require('./config/logger');
const bodyParser = require('./config/body-parser');
const routes = require('./config/routes');
const errors = require('./config/errors');
const passport = require('./config/passport');

models.load();

bodyParser.configure(app);
logger.configure(app);
routes.configure(app, passport);

app.use(staticServe);

errors.handle(app);

module.exports = app;
