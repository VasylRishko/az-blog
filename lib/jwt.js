const JWT = require('jsonwebtoken');
const moment = require('moment');
const config = require('../config');
const secret = config.jwtSecret;

module.exports = {
  sign: function (user) {
    const payload = { id: user.id };
    const options = { expiresIn: config.jwtExpiresIn };
    const exp = moment().add(config.jwtExpiresIn, 'seconds').format('X');

    return {
      token: JWT.sign(payload, secret, options),
      exp: parseInt(exp)
    }
  },

  verify: function (token, cb) {
    return JWT.verify(token, secret, cb)
  }
};
