const request = require('supertest');
const server = require('../server');
const mongoose = require('mongoose');
const { forEach } = require('lodash');

afterEach((done) => {
  clearDB();
  done();
});

after((done) => {
  dropAndCloseDB();
  done()
});

function clearDB () {
  forEach(mongoose.connection.collections, (coll) => {
    coll.remove({ _fixture: { $exists: false } })
  });
}

function dropAndCloseDB () {
  mongoose.connection.db.dropDatabase(() => mongoose.connection.close(done));
}

module.exports = {
  request,
  server
};
