const { expect } = require('chai');
const { request, server } = require('../index');
const { createUser, userToken, checkNotAuthorized } = require('../helper');
const { Post, Comment } = require('mongoose').models;

describe('Posts endpoints', () => {
  let user = {};
  let post = {};
  let comment = {};

  beforeEach((done) => {
    // Create sample data
    createUser()
      .then((u) => {
        user = u;
        return Post.create({
          user: user.id, title: 'Post 1', body: 'lorem ipsum...'
        })
      })
      .then((p) => {
        post = p;
        return Comment.create({
          body: 'lorem ipsum...', user: user.id, post: post.id
        })
      })
      .then((c) => {
        comment = c;
        post.comments.push(comment);
        return post.save()
      })
      .then(() => done())
      .catch(done);
  });

  describe('GET /api/index', () => {
    it('should return 200 with array of posts', (done) => {
      request(server)
        .get('/api/posts')
        .set('accept', 'json')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.posts).to.have.lengthOf(1);
          expect(res.body.posts[0].comments).to.have.lengthOf(1);
          expect(res.body.posts[0].id).to.eq(post.id);
          expect(res.body.posts[0].title).to.eq(post.title);
          expect(res.body.posts[0].body).to.eq(post.body);
          expect(res.body.posts[0].createdAt).to.be.an('string');

          done()
        })
    })
  });

  describe('GET show', () => {
    it('should return 200', (done) => {
      request(server)
        .get(`/api/posts/${post.id}`)
        .set('accept', 'json')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.post.id).to.eq(post.id);
          expect(res.body.post.title).to.eq(post.title);
          expect(res.body.post.body).to.eq(post.body);
          expect(res.body.post.createdAt).to.be.an('string');
          expect(res.body.post.comments).to.have.lengthOf(1);
          expect(res.body.post.comments[0].id).to.eq(comment.id);
          expect(res.body.post.comments[0].body).to.eq(comment.body);
          expect(res.body.post.comments[0].createdAt).to.be.an('string');
          expect(res.body.post.comments[0].user).to.be.an('object');

          done()
        })
    });

    it('should return 404 if post not found', (done) => {
      request(server)
        .get('/api/posts/0')
        .set('accept', 'json')
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('Post not found');
          done()
        })
    })
  });

  describe('PUT edit', () => {
    it('should return 200', (done) => {
      request(server)
        .put(`/api/posts/${post.id}`)
        .send({ title: 'New title', body: 'new body' })
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.post.title).to.eq('New title');
          expect(res.body.post.body).to.eq('new body');

          done()
        })
    });

    it('should return 404 if post not found', (done) => {
      request(server)
        .put('/api/posts/0')
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('Post not found');
          done()
        })
    });

    it('should return 401 if user not authorized', (done) => {
      checkNotAuthorized('PUT', `/api/posts/${post.id}`, done)
    });
  });

  describe('DELETE delete', () => {
    it('should return 401 if user not authorized', (done) => {
      checkNotAuthorized('DELETE', `/api/posts/${post.id}`, done)
    });

    it('should return 200', (done) => {
      request(server)
        .delete(`/api/posts/${post.id}`)
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('Post was deleted');

          done()
        })
    });

    it('should return 404 if post not found', (done) => {
      request(server)
        .delete('/api/posts/0')
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('Post not found');

          done()
        })
    });
  })
});
