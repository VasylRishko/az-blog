const { expect } = require('chai');
const { request, server } = require('../index');
const { createUser, userToken, checkNotAuthorized } = require('../helper');
const { Post, Comment } = require('mongoose').models;

describe('Comments endpoints', function() {
  let user = {};
  let post = {};

  beforeEach((done) => {
    // Create sample data
    createUser()
      .then((u) => {
        user = u;
        Post.create({
          user: user.id, title: 'Post 1', body: 'lorem ipsum...'
        }, (err, p) => {
          post = p;
          done()
        })
      })
  });

  describe('POST create', () => {
    it('should return 401 if user not authorized', (done) => {
      checkNotAuthorized('POST', '/api/comments', done)
    });

    it('should return 200', (done) => {
      request(server)
        .post('/api/comments')
        .send({ body: 'test...', post: post.id })
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.comment.id).to.be.an('string');
          expect(res.body.comment.user).to.eq(user.id);
          expect(res.body.comment.post).to.eq(post.id);
          expect(res.body.comment.body).to.eq('test...');
          expect(res.body.comment.createdAt).to.be.an('string');

          done()
        })
    });

    it('should return 422 if params is invalid', (done) => {
      request(server)
        .post('/api/comments')
        .send({ body: '', post: post.id })
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(422)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.errors.body).to.eq('Text is required');

          done()
        })
    });

    it('should return 404 if post ot found', (done) => {
      request(server)
        .post('/api/comments')
        .send({ body: 'text...', post: '0' })
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('Post not found');

          done()
        })
    });
  });

  describe('DELETE delete', () => {
    let comment = {};

    beforeEach((done) => {
      Comment.create({
        body: 'lorem ipsum...', user: user.id, post: post.id
      }, (err, c) => {
        comment = c;
        done()
      })
    });

    it('should return 401 if user not authorized', (done) => {
      checkNotAuthorized('DELETE', `/api/comments/${comment.id}`, done)
    });

    it('should return 200', (done) => {
      request(server)
        .delete(`/api/comments/${comment.id}`)
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('Comment was deleted');

          done()
        })
    });

    it('should return 404 if comment not found', (done) => {
      request(server)
        .delete('/api/comments/0')
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('Comment not found');

          done()
        })
    });
  });
});
