const { request, server } = require('../index');
const { expect } = require('chai');
const { createUser, userToken, checkNotAuthorized } = require('../helper');

describe('Users endpoints', () => {
  describe('POST create', () => {
    let params = {
      firstName: 'Test',
      lastName: 'User',
      email: 'test@mail.com',
      username: 'testuser',
      password: 'Foobar1$'
    };

    it('should return 200', (done) => {
      request(server)
        .post('/api/users')
        .send(params)
        .set('accept', 'json')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.user.id).to.be.an('string');
          expect(res.body.user.firstName).to.eq('Test');
          expect(res.body.user.lastName).to.eq('User');
          expect(res.body.user.email).to.eq('test@mail.com');
          expect(res.body.user.createdAt).to.be.an('string');
          expect(res.body.user.updatedAt).to.be.an('string');

          done()
        })
    });

    it('should return 422 if params invalid', (done) => {
      params.email = '';
      params.username = '';
      request(server)
        .post('/api/users')
        .send(params)
        .set('accept', 'json')
        .expect(422)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.errors.email).to.eq('Email is required');
          expect(res.body.errors.username).to.eq('Username is required');

          done()
        })
    });
  });

  describe('GET token', () => {
    let user = {};

    beforeEach((done) => {
      createUser((err, u) => {
        user = u;
        done()
      });
    });

    it('should return 200', (done) => {
      request(server)
        .get('/api/users/token')
        .query({ email: user.email, password: 'Foobar1$' })
        .set('accept', 'json')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.jwt.token).to.be.an('string');
          expect(res.body.jwt.exp).to.be.a('number');
          expect(res.body.user.email).to.eq(user.email);

          done()
        })
    });

    it('should return 404 if user not found by email', (done) => {
      request(server)
        .get('/api/users/token')
        .query({ email: 'foobar', password: 'Foobar1$' })
        .set('accept', 'json')
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('User not found');

          done()
        })
    });

    it('should return 422 if params invalid', (done) => {
      request(server)
        .get('/api/users/token')
        .query({ email: user.email, password: '' })
        .set('accept', 'json')
        .expect(422)
        .end((err, res) => {
          if (err) return done(err);

          expect(res.body.message).to.eq('User password is incorrect');

          done()
        })
    });
  });

  describe('GET show', () => {
    let user = {};

    beforeEach((done) => {
      createUser((err, u) => {
        user = u;
        done()
      });
    });

    it('should return 200', (done) => {
      request(server)
        .get('/api/users/me')
        .set('accept', 'json')
        .set('authorization', userToken(user))
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          user = user.toJSON();
          expect(res.body.user.id).to.eq(user.id);
          expect(res.body.user.firstName).to.eq(user.firstName);
          expect(res.body.user.lastName).to.eq(user.lastName);
          expect(res.body.user.email).to.eq(user.email);
          expect(res.body.user.createdAt).to.be.an('string');
          expect(res.body.user.updatedAt).to.be.an('string');

          done()
        })
    });

    it('should return 401 if user not authorized', (done) => {
      checkNotAuthorized('GET', '/api/users/me', done)
    });
  });
});
