const { User } = require('mongoose').models;
const { toLower } = require('lodash');
const { request, server } = require('./index');
const { sign } = require('../lib/jwt');

module.exports = {
  createUser: (attrs, cb) => {
    if (typeof attrs === 'function') {
      cb = attrs;
      attrs = {};
    }

    const defaultAttrs = {
      firstName: 'Test',
      lastName: 'User',
      email: 'test@mail.com',
      username: 'testuser',
      password: 'Foobar1$',
    };
    const createAttrs = Object.assign(defaultAttrs, attrs);

    return User.create(createAttrs, cb);
  },

  userToken: (user) => {
    return `Bearer ${sign(user).token}`
  },

  checkNotAuthorized: (method, path, cb) => {
    method = toLower(method);

    request(server)
      [method](path)
      .set('accept', 'json')
      .set('authorization', 'Bearer invalidtoken')
      .expect(401, cb)
  }
};
