const { request, server } = require('../index');
const { expect } = require('chai');
const User = require('../../app/models/user');
const Post = require('../../app/models/post');

let user = {
  firstName: 'Test',
  lastName: 'User',
  email: 'test@mail.com',
  username: 'testuser',
  password: 'Foobar1$',
};
let validPost = {};

describe('Post model', () => {
  before((done) => {
    User.create(user, (err, u) => {
      user = u;

      done()
    });
  });

  beforeEach((done) => {
    validPost = {
      title: 'Test',
      body: 'lorem ipsum...',
      user: user.id
    };

    done()
  });

  it('should create new post', (done) => {
    Post.create(validPost, (err, result) => {
      expect(err).to.be.null

      expect(result.title).to.eq(validPost.title);
      expect(result.body).to.eq(validPost.body);
      expect(result.user.toString()).to.eq(user.id);
      expect(result.createdAt).to.be.an('date');
      expect(result.updatedAt).to.be.an('date');

      done();
    })
  });

  describe('validations', () => {
    context('#user', () => {
      it('should be present', (done) => {
        validPost.user = null;
        Post.create(validPost, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.user.message).to.eq('User relation is required');
          done();
        })
      });
    });

    context('#title', () => {
      it('should be present', (done) => {
        validPost.title = '';
        Post.create(validPost, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.title.message).to.eq('Title is required');
          done();
        })
      });
    });

    context('#body', () => {
      it('should be present', (done) => {
        validPost.body = '';
        Post.create(validPost, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.body.message).to.eq('Body is required');
          done();
        })
      });
    });
  });
});

