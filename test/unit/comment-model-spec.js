const { expect } = require('chai');
const User = require('../../app/models/user');
const Post = require('../../app/models/post');
const Comment = require('../../app/models/comment');

let user = {
  firstName: 'Test',
  lastName: 'User',
  email: 'test@mail.com',
  username: 'testuser',
  password: 'Foobar1$',
};
let post = {
  title: 'test',
  body: 'lorem ipsum...'
};
let validComment = {};

describe('Comment model', () => {
  before((done) => {
    User.create(user)
      .then((u) => {
        user = u;
        post.user = u.id;

        return Post.create(post)
      })
      .then((p) =>{
        post = p
        done()
      })
  });

  beforeEach((done) => {
    validComment = {
      body: 'lorem ipsum...',
      user: user.id,
      post: post.id
    };

    done()
  });

  it('should create new comment', (done) => {
    Comment.create(validComment, (err, result) => {
      expect(err).to.be.null

      expect(result.body).to.eq(validComment.body);
      expect(result.user.toString()).to.eq(user.id);
      expect(result.post.toString()).to.eq(post.id);
      expect(result.createdAt).to.be.an('date');
      expect(result.updatedAt).to.be.an('date');

      done();
    })
  });

  describe('validations', () => {
    context('#user', () => {
      it('should be present', (done) => {
        validComment.user = null;
        Comment.create(validComment, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.user.message).to.eq('User relation is required');
          done();
        })
      });
    });

    context('#post', () => {
      it('should be present', (done) => {
        validComment.post = null;
        Comment.create(validComment, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.post.message).to.eq('Post relation is required');
          done();
        })
      });
    });

    context('#body', () => {
      it('should be present', (done) => {
        validComment.body = '';
        Comment.create(validComment, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.body.message).to.eq('Text is required');
          done();
        })
      });
    });
  });
});

