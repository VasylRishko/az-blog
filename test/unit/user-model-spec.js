const { expect } = require('chai');
const User = require('../../app/models/user');

let validUser = {};

describe('User model', () => {
  beforeEach((done) => {
    validUser = {
      firstName: 'Test',
      lastName: 'User',
      email: 'test@mail.com',
      username: 'testuser',
      password: 'Foobar1$',
    };

    done()
  });

  it('should create new user', (done) => {
    User.create(validUser, (err, result) => {
      expect(err).to.be.null

      expect(result.firstName).to.eq(validUser.firstName);
      expect(result.lastName).to.eq(validUser.lastName);
      expect(result.email).to.eq(validUser.email);
      expect(result.username).to.eq(validUser.username);
      expect(result.password).to.be.an('string');
      expect(result.createdAt).to.be.an('date');
      expect(result.updatedAt).to.be.an('date');

      done();
    })
  });

  describe('validations', () => {
    context('#username', () => {
      it('should be present', (done) => {
        validUser.username = '';
        User.create(validUser, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.username.message).to.eq('Username is required');
          done();
        })
      });

      it('should be lowercase', (done) => {
        validUser.username = 'soMEuseR';
        User.create(validUser, (err, result) => {
          expect(err).to.be.null
          expect(result.username).to.eq('someuser');
          done();
        })
      });

      it('should trim value', (done) => {
        validUser.username = ' anotheruser    ';
        User.create(validUser, (err, result) => {
          expect(err).to.be.null
          expect(result.username).to.eq('anotheruser');
          done();
        })
      })
    });

    context('#email', () => {
      it('should be present', (done) => {
        validUser.email = '';
        User.create(validUser, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.email.message).to.eq('Email is required');
          done();
        })
      });

      it('should be unique', (done) => {
        const dupUser = Object.assign({}, validUser);

        User
          .create(validUser)
          .then((u) => {
            User.create(dupUser, (err) => {
              expect(err).to.be.an('object');
              expect(err.errors.email.message).to.eq('User this email "test@mail.com" already exists');

              done()
            })
          })
      });

      it('should have valid format', (done) => {
        validUser.email = 'just string';
        User.create(validUser, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.email.message).to.eq('Email is invalid');
          done();
        })
      })
    });

    context('#password', () => {
      it('should be present', (done) => {
        validUser.password = '';
        User.create(validUser, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.password.message).to.eq('Password is required');
          done();
        })
      });

      it('should have 8 symbols minimum', (done) => {
        validUser.password = 'foobar';
        User.create(validUser, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.password.message).to.eq('Password should have 8 symbols minimum');
          done();
        })
      });

      it('should have valid format', (done) => {
        validUser.password = 'foobarbaz';
        User.create(validUser, (err, result) => {
          expect(err).to.be.an('object');
          expect(err.errors.password.message).to.eq('Password requires at least one uppercase letter, one lowercase letter, one number and one special character');
          done();
        })
      })
    })
  });

  describe('pre save', () => {
    it('should encrypt password', (done) => {
      User.create(validUser, (err, result) => {
        expect(err).to.be.null

        expect(result.password).to.not.eq(validUser.password);
        expect(result.password).to.have.lengthOf(60);

        done();
      })
    })
  });

  describe('#comparePassword', () => {
    let user = {};

    beforeEach((done) => {
      User.create(validUser, (err, result) => {
        user = result;
        done()
      });
    });

    it('should return true if password is match', (done) => {
      user.comparePassword('Foobar1$', (err, isMatch) => {
        expect(isMatch).to.eq(true);
        done()
      });
    });

    it('should return true if password is not match', (done) => {
      user.comparePassword('foobar', (err, isMatch) => {
        expect(isMatch).to.eq(false);
        done()
      });
    })
  })
});

